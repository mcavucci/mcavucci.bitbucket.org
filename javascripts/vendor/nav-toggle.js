var smallToggle = $('#small-nav-toggle'),
    smallNav = $('#small-nav'),
    largeToggle = $('#large-nav-toggle'),
    largeNav = $('#large-nav');

    smallToggle.on("click", function() {
        smallNav.toggleClass('open');
    });

    largeToggle.on("click", function() {
        largeNav.toggleClass('open');
    });


